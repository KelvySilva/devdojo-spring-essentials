package br.com.devdojo.awesome.endpoint;

import br.com.devdojo.awesome.model.Student;
import br.com.devdojo.awesome.service.StudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("v1")
public class StudentEndpoint {

    @Autowired
    private StudentService service;

    /**
     * Qualquer desses metodos poderiam receber um parametro do tipo Authentication,
     * que traz as informações de autenticação do usuário da api
     */
    @ApiOperation(value = "Retorna uma lista paginada de Students caso existam Students, caso contrário retorna uma página com lista vazia")
    @GetMapping(path = "protected/students")
    public ResponseEntity<?> listAll(Pageable pageable) {
        return new ResponseEntity<>(service.findStudentsByPage(pageable), HttpStatus.OK);
    }

    @ApiOperation(value = "Retorna uma lista de Students caso haja algum, do contrálio retorna uma lista vazia",response = Student[].class)
    @GetMapping(path = "protected/students/listAll")
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(service.findAllStudents(), HttpStatus.OK);
    }

    @ApiOperation(value = "Retorna o Student pelo id do Student caso ele exista, caso contrário retorna null", response = Student.class)
    @GetMapping(path = "protected/students/{id}")
    public  ResponseEntity<?> getStudentById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.findStudentById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Salva um Student retornado ele em caso de sucesso",response = Student.class)
    @PostMapping(path = "admin/students")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Valid @RequestBody Student student) {

        return new ResponseEntity<>(service.saveStudent(student),HttpStatus.CREATED);
    }

    @ApiOperation(value = "Retorna uma lista com a quantidade de Students que contenha certo nome caso hajam Students, do contrário retorna uma lista vazia", response = Student[].class)
    @GetMapping(path = "protected/students/findByName/{name}")
    public ResponseEntity<?> findStudentByName(@PathVariable("name") String name) {
        return new ResponseEntity<>(service.findStudentsByName(name), HttpStatus.OK);
    }

    @ApiOperation(value = "Retorna status 200 caso o Student exista e tenha sido excluído, caso contrário retorna um ResourceNotFoundException", response = HttpStatus.class)
    @DeleteMapping(path = "admin/students/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> remove(@PathVariable("id") Long id){
        service.deleteStudentById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Atualiza um usuário e retorna Status 200", response = HttpStatus.class)
    @PutMapping(path = "admin/students")
    public ResponseEntity<?> update(@RequestBody Student student) {
        service.updateStudent(student.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
