package br.com.devdojo.awesome.model;

import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Entity
public class Student extends AbstractEntity{

    @NotEmpty
    @NotNull
    private String name;

    @NotEmpty
    @NotNull
    @Email
    private String email;

    public Student(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Student(Long id,String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public Student() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public static Student newInstance(Long id,String name, String email){
        return new Student(id,name,email);
    }

    public static class StudentBuilder {
        protected Long id;
        private String name;
        private String email;

        public StudentBuilder() {
        }

        public StudentBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public StudentBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public StudentBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public Student build() {
            Student student = new Student();
            student.setName(name);
            student.setEmail(email);
            student.setId(id);
            return student;
        }
    }
}
