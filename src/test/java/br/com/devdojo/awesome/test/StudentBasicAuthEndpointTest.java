package br.com.devdojo.awesome.test;


import br.com.devdojo.awesome.ApplicationStart;
import br.com.devdojo.awesome.model.Student;
import br.com.devdojo.awesome.repository.StudentRepository;
import br.com.devdojo.awesome.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc()
public class StudentBasicAuthEndpointTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private StudentService service;

    @LocalServerPort
    int port;

    @MockBean
    private StudentRepository repository;

    /**
     * Não se faz dessa forma, está aqui pois assim foi feito no curso
     */
    @TestConfiguration
    static class Config{

        @Bean
        public RestTemplateBuilder restTemplateBuilder() {
            return new RestTemplateBuilder()
                    .basicAuthentication("spring","spring");
        }
    }


    @Test
    public void listStudentsWhenUsernamAndPasswordAreIncorrectShouldReturnStatusCode401() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        ResponseEntity<String> response = restTemplate.getForEntity("/v1/protected/students/listAll", String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void getStudentsWhenUsernamAndPasswordAreIncorrectShouldReturnStatusCode401() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        ResponseEntity<String> response = restTemplate.getForEntity("/v1/protected/students/1", String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }
    @Test
    public void listStudentsWhenUsernamAndPasswordAreCorrectShouldReturnStatusCode200()  {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L,"Sascoisa","sascoisa@.com.br"),
                new Student(2L,"Sascoisa2","sascoisa2@.com.br")
        );
        BDDMockito.when(service.findAllStudents()).thenReturn(students);
        ResponseEntity<String> response = restTemplate.getForEntity(
                "/v1/protected/students/listAll",
                String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }



    @Test
    public void getStudentWhenUsernamAndPasswordAreCorrectShouldReturnStatusCode200()  {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        Student student = new Student(2L,"Sascoisa2","sascoisa2@.com.br");

        BDDMockito.when(
                service.findStudentById(student.getId())
        ).thenReturn(Optional.of(student));
        ResponseEntity<String> response = restTemplate.getForEntity(
                "/v1/protected/students/2",
                String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void listStudentsShouldReturn2Students()  {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L,"Sascoisa","sascoisa@teste.com.br"),
                new Student(2L,"Sascoisa2","sascoisa2@teste.com.br")
        );
        BDDMockito.when(service.findAllStudents()).thenReturn(students);
        ResponseEntity<Student[]> response =
                restTemplate.getForEntity(
                        "/v1/protected/students/listAll",
                        Student[].class);
        assertThat(Arrays.asList(response.getBody()).size()).isEqualTo(2);
    }

    @Test
    public void getStudentsShouldReturnTheStudent()  {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));

        Student studentOptional = new Student(1L, "teste","teste@email.com");
        BDDMockito.when(service.findStudentById(1L)).thenReturn(Optional.of(studentOptional));

        ResponseEntity<Student> response =restTemplate.getForEntity("/v1/protected/students/{id}",Student.class,1L);
        assertThat(Optional.of(response.getBody())).isEqualTo(Optional.of(studentOptional));
    }

    @Test
    public void getStudentsWhenStudentDoesNotExistShouldReturnStatusCode404()  {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.when(service.findStudentById(-1L)).thenReturn(null);

        ResponseEntity<Student> response =restTemplate.exchange(
                "/v1/protected/students/-1",
                GET,
                null,
                Student.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    public void getStudentsByNameIgnoreCaseShouldReturnOneStudent() {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L,"Primeiro","sascoisa@teste.com.br")
        );
        BDDMockito.when(service.findStudentsByName("Primeiro")).thenReturn(students);
        ResponseEntity<Student[]> response =
                restTemplate.getForEntity("/v1/protected/students/findByName/{name}",Student[].class,"Primeiro");
        assertThat(Arrays.asList(response.getBody()).size()).isEqualTo(1);
    }

    @Test
    public void getStudentsByNameIgnoreCaseShouldReturnTwoStudent() {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L, "Primeiro", "sascoisa@teste.com.br"),
                new Student(1L, "Primeiro 2", "sascoisa@teste.com.br")
        );
        BDDMockito.when(service.findStudentsByName("Primeiro")).thenReturn((students));
        ResponseEntity<Student[]> response =
                restTemplate.getForEntity("/v1/protected/students/findByName/{name}",Student[].class,"Primeiro");
        assertThat(Arrays.asList(response.getBody()).size()).isEqualTo(2);
    }

    @Test
    public void deleteStudentWhenUserHasNotRoleAdminShouldReturnStatusCode403() {
        restTemplate = restTemplate.withBasicAuth("spring", "spring");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.doNothing().when(repository).delete(Student.newInstance(1L,"teste","teste@email.com.br"));
        ResponseEntity<String> response = restTemplate.exchange("/v1/admin/students/{id}", HttpMethod.DELETE, null, String.class, 1L);
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }

    @Test
    public void deleteStudentWhenUserNotExistsShouldReturnStatusCode401() {
        restTemplate = restTemplate.withBasicAuth("teste", "teste");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.doNothing().when(repository).delete(Student.newInstance(1L,"teste","teste@email.com.br"));
        ResponseEntity<String> response = restTemplate.exchange("/v1/admin/students/{id}", HttpMethod.DELETE, null, String.class, -1L);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void deleteStudentWhenUserHasRoleAdminShouldReturnStatusCode200() {
        restTemplate = restTemplate.withBasicAuth("admin", "aj[lo12po");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.doNothing().when(repository).delete(Student.newInstance(1L,"teste","teste@email.com.br"));
        ResponseEntity<String> response = restTemplate.exchange("/v1/admin/students/{id}", HttpMethod.DELETE, null, String.class, 1L);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @WithMockUser(username = "xx",password = "xx",roles = "USER")
    @Test
    public void deleteWhenUserHasRoleUSERShouldReturnStatusCode403() throws Exception {
        BDDMockito.doNothing().when(repository).deleteById(1L);
        mockMvc
                .perform(delete("/v1/admin/students/{id}",-1L))
                .andDo(print())
                .andExpect(status().isForbidden());

    }

    @WithMockUser(username = "xx",password = "xx",roles = "ADMIN")
    @Test
    public void deleteWhenUserHasRoleADMINShouldReturnStatusCode200() throws Exception {
        BDDMockito.doNothing().when(repository).deleteById(1L);
        mockMvc
                .perform(delete("/v1/admin/students/{id}",-1L))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void createWhenNameIsNullShouldReturnStatusCode400() {
        restTemplate = restTemplate.withBasicAuth("admin", "aj[lo12po");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        Student student = new Student(1L,null,"teste@email.com.br");
        BDDMockito
                .when(service.saveStudent(student))
                .thenReturn(student);
        ResponseEntity<String> response = restTemplate.postForEntity("/v1/admin/students", student, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    public void createShouldPersistData() {
        restTemplate = restTemplate.withBasicAuth("admin", "aj[lo12po");
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        Student student = new Student(1L,"teste","teste@email.com.br");
        BDDMockito
                .when(service.saveStudent(student))
                .thenReturn(student);
        ResponseEntity<Student> response = restTemplate.postForEntity("/v1/admin/students", student, Student.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(201);
        assertThat(response.getBody().getName()).contains("teste");
    }
}

