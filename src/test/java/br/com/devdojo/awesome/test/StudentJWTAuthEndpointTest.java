package br.com.devdojo.awesome.test;


import br.com.devdojo.awesome.ApplicationStart;
import br.com.devdojo.awesome.model.Student;
import br.com.devdojo.awesome.repository.StudentRepository;
import br.com.devdojo.awesome.service.StudentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc()
public class StudentJWTAuthEndpointTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private StudentService service;

    @LocalServerPort
    int port;

    @MockBean
    private StudentRepository repository;

    private HttpEntity<Void> protectedHeader;
    private HttpEntity<Void> adminHeader;
    private HttpEntity<Void> wrongHeader;

    @Before
    public void configProtectedHeaders() {
        String str = "{\"username\" :\"spring\",\"password\" :\"spring\"}";
        HttpHeaders headers = restTemplate.postForEntity("/login", str, String.class).getHeaders();
        this.protectedHeader = new HttpEntity<>(headers);
    }
    @Before
    public void configAdminHeaders() {
        String str = "{\"username\" :\"admin\",\"password\" :\"aj[lo12po\"}";
        HttpHeaders headers = restTemplate.postForEntity("/login", str, String.class).getHeaders();
        this.adminHeader = new HttpEntity<>(headers);
    }
    @Before
    public void configWrongHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization","11111");
        this.wrongHeader = new HttpEntity<>(headers);
    }


    @Test
    public void listStudentsWhenTokenIsIncorrectShouldReturnStatusCode403() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        ResponseEntity<String> response = restTemplate.exchange("/v1/protected/students/listAll",GET,wrongHeader, String.class);
        System.out.println("Headers: "+response.getHeaders()+"\nBody: "+response.getBody());
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }

    @Test
    public void getStudentWhenTokenIsIncorrectShouldReturnStatusCode403() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        ResponseEntity<String> response = restTemplate.exchange("/v1/protected/students/1",GET,wrongHeader, String.class);
        System.out.println("Headers: "+response.getHeaders()+"\nBody: "+response.getBody());
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }
    @Test
    public void listStudentsWhenTokenIsCorrectShouldReturnStatusCode200()  {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L,"Sascoisa","sascoisa@.com.br"),
                new Student(2L,"Sascoisa2","sascoisa2@.com.br")
        );
        BDDMockito.when(service.findAllStudents()).thenReturn(students);
        ResponseEntity<String> response = restTemplate.exchange(
                "/v1/protected/students/listAll",
                GET,
                protectedHeader,
                String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void getStudentWhenTokenIsCorrectShouldReturnStatusCode200()  {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        Student student = new Student(2L,"Sascoisa2","sascoisa2@.com.br");

        BDDMockito.when(
                service.findStudentById(student.getId())
        ).thenReturn(Optional.of(student));
        ResponseEntity<String> response = restTemplate.exchange(
                "/v1/protected/students/2",
                GET,
                protectedHeader,
                String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void listStudentsShouldReturn2Students()  {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L,"Sascoisa","sascoisa@teste.com.br"),
                new Student(2L,"Sascoisa2","sascoisa2@teste.com.br")
        );
        BDDMockito.when(service.findAllStudents()).thenReturn(students);
        ResponseEntity<Student[]> response =
                restTemplate.exchange(
                        "/v1/protected/students/listAll",
                        GET,
                        protectedHeader,
                        Student[].class);
        assertThat(Arrays.asList(response.getBody()).size()).isEqualTo(2);
    }

    @Test
    public void getStudentsShouldReturnTheStudent()  {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));

        Student studentOptional = new Student(1L, "teste","teste@email.com");
        BDDMockito.when(service.findStudentById(1L)).thenReturn(Optional.of(studentOptional));

        ResponseEntity<Student> response =restTemplate.exchange(
                "/v1/protected/students/{id}",
                GET,
                protectedHeader,
                Student.class,1L);
        assertThat(Optional.of(response.getBody())).isEqualTo(Optional.of(studentOptional));
    }

    @Test
    public void getStudentsWhenStudentDoesNotExistShouldReturnStatusCode404()  {
        BDDMockito.when(service.findStudentById(-1L)).thenReturn(null);
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));

        ResponseEntity<Student> response =restTemplate.exchange(
                "/v1/protected/students/-1",
                GET,
                protectedHeader,
                Student.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    public void getStudentsByNameIgnoreCaseShouldReturnOneStudent() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L,"Primeiro","sascoisa@teste.com.br")
        );
        BDDMockito.when(service.findStudentsByName("Primeiro")).thenReturn(students);
        ResponseEntity<Student[]> response =
                restTemplate.exchange(
                        "/v1/protected/students/findByName/{name}",
                        GET,
                        protectedHeader,
                        Student[].class,
                        "Primeiro");
        assertThat(Arrays.asList(response.getBody()).size()).isEqualTo(1);
    }

    @Test
    public void getStudentsByNameIgnoreCaseShouldReturnTwoStudent() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        List<Student> students = asList(
                new Student(1L, "Primeiro", "sascoisa@teste.com.br"),
                new Student(1L, "Primeiro 2", "sascoisa@teste.com.br")
        );
        BDDMockito.when(service.findStudentsByName("Primeiro")).thenReturn((students));
        ResponseEntity<Student[]> response =
                restTemplate.exchange(
                        "/v1/protected/students/findByName/{name}",
                        GET,
                        protectedHeader,
                        Student[].class,
                        "Primeiro");
        assertThat(Arrays.asList(response.getBody()).size()).isEqualTo(2);
    }

    @Test
    public void deleteStudentWhenUserHasNotRoleAdminShouldReturnStatusCode403() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.doNothing().when(repository).delete(Student.newInstance(1L,"teste","teste@email.com.br"));
        ResponseEntity<String> response = restTemplate.exchange(
                "/v1/admin/students/{id}",
                HttpMethod.DELETE, protectedHeader,
                String.class,
                1L);
        System.out.println("Header: "+response.getHeaders()+"\nBody: "+response.getBody());
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }

    @Test
    public void deleteStudentWhenUserNotExistsShouldReturnStatusCode403() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.doNothing().when(repository).delete(Student.newInstance(1L,"teste","teste@email.com.br"));
        ResponseEntity<String> response = restTemplate.exchange(
                "/v1/admin/students/{id}",
                HttpMethod.DELETE,
                wrongHeader,
                String.class,
                1L);
        System.out.println("Header: "+response.getHeaders()+"\nBody: "+response.getBody());
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }

    @Test
    public void deleteStudentWhenEndpointIsIncorrectShouldReturnStatusCode405() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.doNothing().when(repository).delete(Student.newInstance(1L,"teste","teste@email.com.br"));
        ResponseEntity<String> response = restTemplate.exchange(
                "/v1/protected/students/{id}",
                HttpMethod.DELETE,
                adminHeader,
                String.class,
                1L);
        System.out.println("Header: "+response.getHeaders()+"\nBody: "+response.getBody());
        assertThat(response.getStatusCodeValue()).isEqualTo(405);
    }

    @Test
    public void deleteStudentWhenUserHasRoleAdminShouldReturnStatusCode200() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        BDDMockito.doNothing().when(repository).delete(Student.newInstance(1L,"teste","teste@email.com.br"));
        ResponseEntity<String> response = restTemplate.exchange(
                "/v1/admin/students/{id}",
                HttpMethod.DELETE,
                adminHeader,
                String.class,
                1L);
        System.out.println("Header: "+response.getHeaders()+"\nBody: "+response.getBody());
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void deleteWhenUserHasRoleUSERShouldReturnStatusCode403() throws Exception {
        String token = protectedHeader.getHeaders().get("Authorization").get(0);
        BDDMockito.doNothing().when(repository).deleteById(-1L);
        mockMvc
                .perform(
                        delete("/v1/admin/students/{id}",-1L)
                        .header("Authorization",token)
                )
                .andDo(print())
                .andExpect(status().isForbidden());

    }

    @Test
    public void deleteWhenUserHasRoleADMINShouldReturnStatusCode200() throws Exception {
        String token = adminHeader.getHeaders().get("Authorization").get(0);
        BDDMockito.doNothing().when(repository).deleteById(1L);
        mockMvc
                .perform(
                        delete("/v1/admin/students/{id}",1L)
                        .header("Authorization",token)
                )
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void createWhenNameIsNullShouldReturnStatusCode400() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        Student student = new Student(1L,null,"teste@email.com.br");
        BDDMockito
                .when(service.saveStudent(student))
                .thenReturn(student);
        ResponseEntity<String> response = restTemplate.exchange(
                "/v1/admin/students",
                POST,
                new HttpEntity<>(student,adminHeader.getHeaders()),
                String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    public void createShouldPersistData() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        Student student = new Student(1L,"teste","teste@email.com.br");
        BDDMockito
                .when(service.saveStudent(student))
                .thenReturn(student);
        ResponseEntity<Student> response = restTemplate.exchange(
                "/v1/admin/students",
                POST,
                new HttpEntity<>(student, adminHeader.getHeaders()),
                Student.class);
        System.out.println("Headers: "+response.getHeaders()+"\nBody: "+response.getBody());
        assertThat(response.getStatusCodeValue()).isEqualTo(201);
        assertThat(response.getBody().getName()).contains("teste");
    }
}

