package br.com.devdojo.awesome.javaclient;

import br.com.devdojo.awesome.handler.RestResponseExceptionHandler;
import br.com.devdojo.awesome.util.RequestResponseLoggingInterceptor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class JavaSpringClientTest {

    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplateBuilder()
                .rootUri("http://localhost:8080/v1/protected/students")
                .errorHandler(new RestResponseExceptionHandler())
                .basicAuthentication("spring","spring").build();

        RestTemplate restTemplateAdmin = new RestTemplateBuilder()
                .rootUri("http://localhost:8080/v1/admin/students")
                .errorHandler(new RestResponseExceptionHandler())
                .basicAuthentication("admin","aj[lo12po").build();

        restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        restTemplateAdmin.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
        /**
         * Forma 1)
         */
        /*
        Student firstStudent  = restTemplate.getForObject("/{id}",Student.class,1);
        */

        /**
         * Forma 2)
         */
        /*
        ResponseEntity<Student> entityStudent = restTemplate.getForEntity("/{id}", Student.class,1);
        Student student = entityStudent.getBody();
        */


        /**
         * List all
         */
        /*
        ResponseEntity<PageableResponse<Student>> exchange = restTemplate.exchange("/?page=0",HttpMethod.GET,null,
                new ParameterizedTypeReference<PageableResponse<Student>>() {
                });

        List<Student> all = exchange.getBody().getContent();
        System.out.println("allStudents 1)");
        all.forEach(studentOne -> System.out.println(studentOne.toString()));
        System.out.println("allStudents 2)");
        Consumer<Student> consumer = x -> System.out.println(x.toString());
        all.forEach(consumer);
         */


        /*
        Student studentPost = new Student();
        studentPost.setName("Um nome");
        studentPost.setEmail("email@qualquer.com");
         */

        /**
        *Primeira forma de fazer, utilizando exchange
        */
        /*
        ResponseEntity<Student> exchangePost = restTemplateAdmin.exchange("/",HttpMethod.POST,
                new HttpEntity<>(studentPost, crateJsonHeader()),Student.class);
                * */

        /**
         * Segunda forma de fazer, utilizando postForObject
         */
        /*
        Student studentPost2 = new Student();
        studentPost2.setName("Outro nome");
        studentPost2.setEmail("email@outro.com");
        Student studentResponse = restTemplateAdmin.postForObject("/", studentPost2, Student.class);
        System.out.println(studentResponse);
         */


        /**
         * Terceira forma de fazer utilizando postForEntity
         */
       /* Student studentPost3 = new Student();
        studentPost3.setName("Mais um nome");
        studentPost3.setEmail("maisum@email.com");
        ResponseEntity<Student> studentResponseEntity = restTemplateAdmin.postForEntity("/", studentPost3, Student.class);
        System.out.println(studentResponseEntity);
        */

        JavaClientDao dao = new JavaClientDao();
        dao.findById(1111L);

    }


}
