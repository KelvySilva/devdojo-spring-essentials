package br.com.devdojo.awesome.javaclient;

import br.com.devdojo.awesome.handler.RestResponseExceptionHandler;
import br.com.devdojo.awesome.model.PageableResponse;
import br.com.devdojo.awesome.model.Student;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.OptionalInt;

public class JavaClientDao {

   private RestTemplate restTemplate = new RestTemplateBuilder()
            .rootUri("http://localhost:8080/v1/protected/students")
            .errorHandler(new RestResponseExceptionHandler())
            .basicAuthentication("spring","spring").build();

   private RestTemplate restTemplateAdmin = new RestTemplateBuilder()
            .rootUri("http://localhost:8080/v1/admin/students")
            .errorHandler(new RestResponseExceptionHandler())
            .basicAuthentication("admin","aj[lo12po").build();

   public Student findById(Long id) {
       return restTemplate.getForObject("/{id}",Student.class,id);
   }

   public List<Student> findAll() {
       ResponseEntity<PageableResponse<Student>> exchange = restTemplate.exchange("/", HttpMethod.GET, null,
               new ParameterizedTypeReference<PageableResponse<Student>>() {
               });
       return exchange.getBody().getContent();
   }

    public List<Student> listAll() {
        ResponseEntity<List<Student>> exchange = restTemplate.exchange("/listAll", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Student>>() {
                });
        return exchange.getBody();
    }

   public Student save(Student student) {
        return restTemplateAdmin.postForObject("/", student, Student.class);
   }
   public void update(Student student) {
       restTemplateAdmin.put("/", student);
   }
    public void delete(Long id) {
        restTemplateAdmin.delete("/{id}", id);
    }

    private static HttpHeaders crateJsonHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

}
