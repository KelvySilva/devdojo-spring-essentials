package br.com.devdojo.awesome.javaclient;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class JavaClientTest {
    public static void main(String[] args) {

        String user = "spring";
        String pass = "spring";

        HttpURLConnection connection = null;

        BufferedReader reader = null;

        try {
            URL url = new URL("http://localhost:8080/v1/protected/students/");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.addRequestProperty("Authorization","Basic "+encodeAuth(user,pass));
            System.out.println(encodeAuth(user,pass));
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder jsonSB = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                jsonSB.append(line);
            }
            System.out.println(jsonSB.toString());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            IOUtils.closeQuietly(reader);
            if (connection != null) connection.disconnect();
        }
    }

    private static String encodeAuth(String user, String password) {
        String auth = user+":"+password;
        return new String(Base64.encodeBase64(auth.getBytes()));
    }
}
