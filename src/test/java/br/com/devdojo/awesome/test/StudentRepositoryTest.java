package br.com.devdojo.awesome.test;


import br.com.devdojo.awesome.ApplicationStart;
import br.com.devdojo.awesome.model.Student;
import br.com.devdojo.awesome.repository.StudentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)
@TestPropertySource("classpath:application-test.properties")
public class StudentRepositoryTest {

    @Autowired
    private StudentRepository repository;

    @Test
    public void createShouldPersistData() {
        Student student = new Student("novo teste","testejunit@teste.com");
        repository.save(student);
        assertThat(student.getId()).isNotNull();
        assertThat(student.getName()).isEqualTo("novo teste");
        assertThat(student.getEmail()).isEqualTo("testejunit@teste.com");
    }

    @Test
    public void deleteShouldRemoveData(){
        Student student = new Student("novo teste","testejunit@teste.com");
        repository.save(student);
        repository.delete(student);
        assertThat(repository.findById(student.getId())).isNotPresent();
    }

    @Test
    public void updateShouldChangeAndPersistData(){
        Student student = new Student("mais novo estudante","mne@email.com");
        repository.save(student);
        student.setName("Outro novo estudante");
        student.setEmail("one@email.com");
        repository.save(student);
        student = (repository.findById(student.getId())).get();
        if(repository.findById(student.getId()).isPresent()) {
            student = repository.findById(student.getId()).get();
        }
        assertThat(student.getName()).isEqualTo("Outro novo estudante");
        assertThat(student.getEmail()).isEqualTo("one@email.com");

    }

    @Test
    public void findByNameIgnoreCaseContainingShouldIgnoreCase(){
        Student student1 = new Student("Kelvy","mne@email.com");
        Student student2 = new Student("kelvy","mne@teste.com");
        repository.save(student1);
        repository.save(student2);

        List<Student> students = repository.findByNameIgnoreCaseContaining("kelvy");
        assertThat(students.size()).isEqualTo(2);
    }

    @Test
    public void createWhenNameIsNullShouldThrowsConstraintViolationException() {
        Student student = new Student();
        assertThatThrownBy(() -> {
            repository.save(student);
        }).isInstanceOf(ConstraintViolationException.class);
    }
    @Test
    public void createWhenEmailIsNullShouldThrowsConstraintViolationException() {
        Student student = new Student();
        student.setName("Novo nome");
        assertThatThrownBy(() -> {
            repository.save(student);
        }).isInstanceOf(ConstraintViolationException.class);
    }
}
