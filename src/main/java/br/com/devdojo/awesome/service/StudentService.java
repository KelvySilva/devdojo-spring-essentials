package br.com.devdojo.awesome.service;

import br.com.devdojo.awesome.error.ResourceNotFoundException;
import br.com.devdojo.awesome.model.Student;
import br.com.devdojo.awesome.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository repository;

    public Optional<Student> findStudentById(Long id) {
        studentExists(id);
        return repository.findById(id);
    }

    public Student getStudentOne(Long id) {
        return  repository.getOne(id);
    }

    public Iterable<Student> findAllStudents() {
        return repository.findAll();
    }

    public Page<Student> findStudentsByPage(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public void deleteStudentById(Long id) {
        studentExists(id);
        repository.deleteById(id);
    }

    public void updateStudent(Long id) {
        repository.findById(id).ifPresent(s -> {
            repository.save(s);
        });
    }

    public List<Student> findStudentsByName(String name) {
        return repository.findByNameIgnoreCaseContaining(name);
    }

    public Student saveStudent(Student student) {
       return repository.save(student);
    }
    public void studentExists(Long id) {
        if (repository.findById(id) == null) throw new ResourceNotFoundException("Student not found for id: "+id);
    }
}
